---
bookCollapseSection: true
weight: 11
---

# Calendar Subscription

{{< hint info >}}
<b> <a href ="{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/#add-a-policy"> Add policy </a></b>in Policy description page gives a brief description how a policy is added to an Android device.
{{< /hint >}}

This configuration can be used to define settings for calendar subscriptions. Once this configuration profile is installed on an iOS device, corresponding users will not be able to modify these settings on their devices.


<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
        </tr>
        <tr>
            <td><strong>Description</strong></td>
            <td>Description of the account.
            </td>
        </tr>
        <tr>
            <td><strong>Account Hostname URL</strong></td>
            <td>The server address.</td>
        </tr>
        <tr>
            <td><strong>Use Secure Socket Layer(SSL)</strong></td>
            <td>Having this checked, would enable Secure Socket Layer communication.
            </td>
        </tr>
        <tr>
            <td><strong>Username</strong></td>
            <td>The userʼs login name.</td>
        </tr>
        <tr>
            <td><strong>Password</strong></td>
            <td>The userʼs password.</td>
        </tr>
    </tbody>
</table>

{{< hint info >}}
<b> <a href = "{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/#publish-a-policy"> Publish a policy </a></b>in Policy description page gives a brief description how a policy is published.
 {{< /hint >}}