---
bookCollapseSection: true
weight: 2
---

# Restrictions

{{< hint info >}}
<b> <a href ="https://entgra-documentation.gitlab.io/v3.7
.0/docs/guide-to-work-with-the-product/device-management-guide/android-device/android-device-policies/#add-a-policy"> Add policy </a></b>in Policy description page gives a brief description how a policy is added to an Android device.
{{< /hint >}}

This configurations can be used to restrict certain settings on an Android device. Once this configuration profile is installed on a device, corresponding users will not be able to modify these settings on their devices.

<strong> Disallow configuring credentials </strong>

This configuration makes the user not able to configure the certificates in the security certificates. To test this in work profile, enforce this policy on the device and in the device go to the settings. 

<ol>
<li>Search for certificates, and click on view security certificates.</li>

<img src ="configuringCredentials.png" style="border:5px solid black ">

<li>If you tap on certificates under personal, a popup will appear allowing you to turn off a certificate.</li>

<img src ="configuringCredentials2.png" style="border:5px solid black ">

<li>Since the policy is applied on work profile, the turn off button is not visible.</li>

<img src ="configuringCredentials3.png" style="border:5px solid black ">
</ol>

<strong> Disallow configuring VPN </strong>

This will disallow configuring VPN configs. To test this in the work profile, download a free VPN app for example “turbo VPN” from play store and try to create a VPN connection. The application will show an error. 

<strong> Disallow configuring app control </strong> 

Specifies if a user is disallowed from modifying applications in Settings or launchers. The following actions will not be allowed when this restriction is enabled:

<li>uninstalling apps</li>

<li>disabling apps</li>

<li>clearing app caches</li>

<li>clearing app data</li>

<li>force stopping apps</li>

<li>clearing app defaults</li>

After applying the policy, try to remove the installed app for example the VPN client from the device and it will not work.

<img src ="DisallowConfiguringAppontrol.png" style="border:5px solid black ">

In the first image, uninstall is disabled, and the second image is from settings -> apps -> installed app. Click on uninstall/force stop or under application’s storage, click on clear data or clear cache. The policy will prevent from doing so.

<strong> Disallow crossprofile copy paste </strong>

This will disable the text copy-pasting between work profile and the regular profile. To test, go to an app where you can type text for example, in the google playstore’s search box and type some text and copy it. Go to another app in the phone which is not a part of the work profile and try to paste. The text will not be available. Remove the policy and retry. 

<strong> Disallow debugging </strong>

This will disable the ability to debug any application through the Android studio. 

<strong> Disallow install apps </strong>

Try to install an app using google play store on the work profile. This will not be allowed and  

<strong> Disallow install from unknown sources </strong>

Install a browser such as chrome in the work profile and try to install an apk file from untrusted source such as

 https://github.com/selendroid/selendroid/raw/master/selendroid-standalone/src/test/resources/selendroid-test-app.apk 

This will be prevented by the OS.

<strong> Disallow modify accounts </strong>

Go to Settings -> “Cloud and accounts” -> “accounts”
Under work, the add account will be disabled. Also if you go inside the existing Google account of the work profile and click on the 3 dot menu on the top right corner, the remove button is greyed out(Disabled). 

<strong> Disallow outgoing beams </strong>

This is related to NFC beams. Install an NFC app that sends some information to another. for example NFC contacts application into the work profile and try to send NFC beam to another NFC enabled device.

<strong> Disallow location sharing </strong>

This restriction disallows turning on location sharing.
Under settings -> location  -> the work profile and regular profiles location control is available.

<strong> Disallow uninstall apps </strong>

After enabling this restriction, install an app through the work profile and try to uninstall it and it is not allowed.

<strong> Ensure verifying apps </strong>

In settings, go to Google -> Work -> Security -> Verify apps -> Scan device for security threats is not configurable.

<strong> Enable auto timing </strong>

Search for automatic date and time in settings of the device and enabling/disabling this setting is controlled by this setting.

<strong> Disable screen capture </strong>

This will disable the ability to take screenshots by pressing the power key together with volume down.

{{< hint info >}}
Following set of restrictions require the device to be in device owner mode.
Follow the <b><a href ="https://entgra-documentation.gitlab.io/v3.7
.0/docs/guide-to-work-with-the-product/device-management-guide/android-device/android-device-policies/#add-a-policy"> documentation </a></b> to get the device into device owner mode.
{{< /hint >}}

<strong> Disallow SMS </strong>

The user will not be allowed to send or receive SMS.

<strong> Disallow volume adjust </strong>

This will disable the ability changing device volume. so the volume keys must not have an effect.

<strong> Disallow cell broadcast </strong>

Disables cell broadcasting messages (message -> Settings -> Cell Broadcast).

<strong> Disallow configuring Bluetooth </strong>

This restriction will Disable the Bluetooth settings in settings of the device.

<strong> Disallow configuring mobile networks </strong>

Under settings -> mobile networks -> mobile configurations menu disabling is achieved with this configuration.

<strong> Disallow configuring tethering </strong>

This will disable the ability to configure mobile hotspots and tethering which is found in settings.

<strong> Disallow configuring WiFi </strong>

Disable the ability to configure wifi settings in device settings.

<strong> Disallow safe boot </strong>

Disables the ability to safe boot a device to remove any apps installed.
https://support.t-mobile.com/docs/DOC-34283

<strong> Disallow outgoing calls </strong>

The user is not allowed to make outgoing phone calls. Emergency calls are still permitted.

<strong> Disallow mount physical media </strong>

The user is disallowed from mounting physical external media. Connect a Pendrive to the device via the OTG cable and the device will not allowed to mount Pendrive.

<strong> Disallow create window </strong>

Apps running are not allowed to create following <a href ="https://developer.android.com/reference/android/os/UserManager.html#DISALLOW_CREATE_WINDOWS"> types </a></b> of windows.

<ul>
    <li>LayoutParams#TYPE_TOAST</li>
    <li>LayoutParams#TYPE_PHONE</li>
    <li>LayoutParams#TYPE_PRIORITY_PHONE</li>
    <li>LayoutParams#TYPE_SYSTEM_ALERT</li>
    <li>LayoutParams#TYPE_SYSTEM_ERROR</li>
    <li>LayoutParams#TYPE_SYSTEM_OVERLAY</li>
    <li>LayoutParams#TYPE_APPLICATION_OVERLAY</li>
</ul>

To test, have an application that creates a toast and for example in the VPN app, when the VPN is created a toast may be shown and this must be disallowed with this setting.

<img src ="DisallowCreteWindow.png" style="border:5px solid black ">

<strong> Disallow factory reset </strong>

Disabled the ability to factory set the device. Go to settings on the device, and the factory reset must be disabled.

<strong> Disallow remove user / Disallow add user </strong>

Multiple user profile control. The users are not allowed to remove or add. This may not be <a href ="https://support.google.com/nexus/answer/2865483?hl=en"> available </a></b> in some Android devices.

<strong> Disallow network reset </strong>

This restriction will disallow network resetting and to check, type "reset network settings" in the settings search bar and the reset settings will be disabled.

<strong> Disallow USB file transfer </strong>

This restriction will disallow the file transfer via USB.

<strong> Disallow factory reset </strong>

Disabled the ability to factory set the device. Go to settings on the device, and the factory reset must be disabled.

<strong> Disallow unmute microphone </strong>

This restriction will disable the microphone. Check the device microphone by using a recording app.

<strong> Disallow status bar </strong>

This restriction will disable the device status bar.

<img src ="statusBar.png" style="border:5px solid black ">

<strong> Disallow autofill </strong>

This restriction will disable autofill services.To check, install a application that needs to enter user credintials to log in. when entering the credintials, device autofill service will ask to save credincials to autofill service(samsung devices uses samsung pass as default).After when this restiriction active,this auto fill service will be disabled.

<img src ="autoFill.jpg" style="border:5px solid black width:500px height:600px">

{{< hint info >}}
<b> <a href = "https://entgra-documentation.gitlab.io/v3.7
.0/docs/guide-to-work-with-the-product/device-management-guide/android-device/android-device
-policies/#publish-a-policy"> Publish a policy </a></b>in Policy description page gives a brief description how a policy is published.
 {{< /hint >}}