---
bookCollapseSection: true
weight: 5
---

# Global Proxy Settings

{{< hint info >}}
<b> <a href ="https://entgra-documentation.gitlab.io/v3.7
.0/docs/guide-to-work-with-the-product/device-management-guide/android-device/android-device-policies/#add-a-policy"> Add policy </a></b>in Policy description page gives a brief description how a policy is added to an Android device.
{{< /hint >}}


This configurations can be used to set a network-independent global HTTP proxy on an Android device. Once this configuration profile is installed on a device, all the network traffic will be routed through the proxy server.

 <i>This profile requires the agent application to be the device owner.<br>
 This proxy is only a recommendation and it is possible that some apps will ignore it.</i>

<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td colspan="2">
                <center><strong>Proxy Configuration Type</strong></center>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <center><i><strong>Manual</strong></i></center>
            </td>
        </tr>
        <tr>
            <td><strong>Proxy Host</strong></td>
            <td>Host name/IP address of the proxy server.
                <br>Eg:[ 192.168.8.1 ]</td>
        </tr>
        <tr>
            <td><strong>Proxy Port</strong></td>
            <td>Target port for the proxy server.
                <br> Eg:[ Target port 0-65535 ]</td>
        </tr>
        <tr>
            <td><strong>Proxy Exclusion List </strong></td>
            <td>Add hostnames to this separated by commas to prevent them from routing through the proxy server. The hostname entries can be wildcards such as *.example.com
                <br>Eg:[ localhost, *.example .com ]</td>
        </tr>
        <tr>
            <td colspan="2">
                <center><i><strong>Auto</strong></i></center>
            </td>
        </tr>
        <tr>
            <td><strong>Proxy PAC File URL </strong></td>
            <td>URL for the proxy auto config PAC script
                <br> Eg: [ http://exampleproxy.com/proxy.pac ]
            </td>
        </tr>
    </tbody>
</table>

{{< hint info >}}
<b> <a href = "https://entgra-documentation.gitlab.io/v3.7
.0/docs/guide-to-work-with-the-product/device-management-guide/android-device/android-device
-policies/#publish-a-policy"> Publish a policy </a></b>in Policy description page gives a brief description how a policy is published.
 {{< /hint >}}